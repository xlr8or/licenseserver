<div class="licenses form">
<?php echo $this->Form->create('License'); ?>
	<fieldset>
		<legend><?php echo __('Admin Add License'); ?></legend>
	<?php
		echo $this->Form->input('license_key');
		echo $this->Form->input('first_name');
		echo $this->Form->input('last_name');
		echo $this->Form->input('email');
		echo $this->Form->input('website');
		echo $this->Form->input('time_add');
		echo $this->Form->input('time_expire');
		echo $this->Form->input('time_edit');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Licenses'), array('action' => 'index')); ?></li>
	</ul>
</div>
