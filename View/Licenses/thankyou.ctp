<?php
/**
 * XLRstats : Real Time Player Stats (http://www.xlrstats.com)
 * (CC) BY-NC-SA 2005-2013, Mark Weirath, Özgür Uysal
 *
 * Licensed under the Creative Commons BY-NC-SA 3.0 License
 * Redistributions of files must retain the above copyright notice.
 *
 * @link          http://www.xlrstats.com
 * @license       Creative Commons BY-NC-SA 3.0 License (http://creativecommons.org/licenses/by-nc-sa/3.0/)
 * @package       app.
 * @since         XLRstats v3.0
 * @version       0.1
 */

 ?>

<div class="page-header">
	<h1>Thank you for choosing XLRstats v3.</h1>
</div>
<div class="row">
	<div class="span9">
		If you feel that this product is valuable to you and your community, please consider making a donation to the B3
		fund so we can continue developing Open Source Software.
	</div>
	<div class="span3">
		<?php echo $this->element('donate_button'); ?>
	</div>
</div>