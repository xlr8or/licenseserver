<div class="licenses view">
<h2><?php echo __('License'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($license['License']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('License Key'); ?></dt>
		<dd>
			<?php echo h($license['License']['license_key']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('First Name'); ?></dt>
		<dd>
			<?php echo h($license['License']['first_name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Last Name'); ?></dt>
		<dd>
			<?php echo h($license['License']['last_name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Email'); ?></dt>
		<dd>
			<?php echo h($license['License']['email']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Website'); ?></dt>
		<dd>
			<?php echo h($license['License']['website']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Time Add'); ?></dt>
		<dd>
			<?php echo h($license['License']['time_add']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Time Expire'); ?></dt>
		<dd>
			<?php echo h($license['License']['time_expire']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Time Edit'); ?></dt>
		<dd>
			<?php echo h($license['License']['time_edit']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit License'), array('action' => 'edit', $license['License']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete License'), array('action' => 'delete', $license['License']['id']), null, __('Are you sure you want to delete # %s?', $license['License']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Licenses'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New License'), array('action' => 'add')); ?> </li>
	</ul>
</div>
