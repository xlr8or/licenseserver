<div class="licenses form">
<?php echo $this->Form->create('License'); ?>
	<fieldset>
		<legend><?php echo __('Acquire a License Key'); ?></legend>
		<blockquote>Using this form you can acquire a License Key for XLRstats v3. The key will be emailed to the address provided. </blockquote>
	<?php
//		echo $this->Form->input('license_type', array(
//			'options' => array('F' => 'Free', 'C' => 'Commercial'),
//			'empty' => false,
//			'value' => 'F'
//		));
		echo $this->Form->input('first_name');
		echo $this->Form->input('last_name');
		echo $this->Form->input('email');
		echo $this->Form->input('website');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
