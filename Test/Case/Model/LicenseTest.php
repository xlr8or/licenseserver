<?php
App::uses('License', 'LicenseServer.Model');

/**
 * License Test Case
 *
 */
class LicenseTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'plugin.license_server.license'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->License = ClassRegistry::init('LicenseServer.License');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->License);

		parent::tearDown();
	}

}
