<?php
App::uses('LicenseServerAppController', 'LicenseServer.Controller');
App::uses('CakeTime', 'Utility');
App::uses('CakeEmail', 'Network/Email');
App::import('Vendor', 'LicenseServer.IpnListener', array('file' => 'PHP-PayPal-IPN' . DS . 'ipnlistener.php'));
/**
 * Licenses Controller
 *
 * @property License $License
 * @property PaginatorComponent $Paginator
 */
class LicensesController extends LicenseServerAppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

	public function beforeFilter() {
		parent::beforeFilter();
		$this->layout = 'xlrstats.com';
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {

		if ($this->request->is('post')) {
			$type = $this->request->pass[0];
			if ($type == '') {
				$type = 'free';
			}
			elseif ($type != 'free' && $type != 'commercial') {
				$this->Session->setFlash(__('Invalid license request. Please, try again.'));
				return false;
			}
			// Generate a license and make sure there is no duplicate in the database...
			do {
				$key = $this->generate_key();
				$notUnique = $this->License->findByLicenseKey($key);
			} while (!empty($notUnique));

			$this->License->create();
			$data = $this->request->data;
			$data['License']['license_key'] = $this->generate_key();
			$data['License']['time_add'] = date("Y-m-d H:i:s");
			//pr($data);
			if ($this->License->save($data)) {
				$bcc = false;
				$msg = "Hi " . $data['License']['first_name'] . ", thank you for using XLRstats v3!\n\n" ;
				if ($type == 'commercial') {
					$msg .= "You requested a Commercial Key, we will be in contact with you soon. For the time being you'll be using a free key.\n\n";
					$bcc = true;
				}
				$msg .= "Your License Key for XLRstats v3 is: " . $data['License']['license_key'];
				$msg .= "\n\nThe XLRstats Team.";
				$Email = new CakeEmail(array('from' => 'licenses@xlrstats.com', 'transport' => 'Mail'));
				if ($bcc) {
					$Email->addBcc(array('license@xlrstats.com'));
				}
				$Email->from(array('licenses@xlrstats.com' => 'XLRstats License Server'))
					->to($data['License']['email'])
					->subject('[!] Your XLRstats License Key')
					->send($msg);

				$this->Session->setFlash(__('The license has been saved and an email containing the key was sent to the address you provided.'));
				return $this->redirect(array('action' => 'thankyou'));
			} else {
				$this->Session->setFlash(__('The license could not be saved. Please, try again.'));
			}
		}
	}

	/**
	 *
	 */
	public function thankyou() {

	}

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->License->recursive = 0;
		$this->set('licenses', $this->Paginator->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->License->exists($id)) {
			throw new NotFoundException(__('Invalid license'));
		}
		$options = array('conditions' => array('License.' . $this->License->primaryKey => $id));
		$this->set('license', $this->License->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->License->create();
			if ($this->License->save($this->request->data)) {
				$this->Session->setFlash(__('The license has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The license could not be saved. Please, try again.'));
			}
		}
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->License->exists($id)) {
			throw new NotFoundException(__('Invalid license'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->License->save($this->request->data)) {
				$this->Session->setFlash(__('The license has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The license could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('License.' . $this->License->primaryKey => $id));
			$this->request->data = $this->License->find('first', $options);
		}
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->License->id = $id;
		if (!$this->License->exists()) {
			throw new NotFoundException(__('Invalid license'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->License->delete()) {
			$this->Session->setFlash(__('The license has been deleted.'));
		} else {
			$this->Session->setFlash(__('The license could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}

	/**
	 * @return string
	 */
	public function generate_key()
	{
		$pre = 'XLR3-';
		$post = '';
		$template   = 'XXX9-XX9X-X99X-9XXX-XXXX';
		$k = strlen($template);
		$sernum = '';
		for ($i=0; $i<$k; $i++)
		{
			switch($template[$i])
			{
				case 'X': $sernum .= chr(rand(65,90)); break;
				case '9': $sernum .= rand(0,9); break;
				case '-': $sernum .= '-';  break;
			}
		}
		return $pre . $sernum . $post;
	}

	/**
	 * @param string $key
	 * @return bool
	 */
	public function check($key='') {
		$this->layout = null;

		// Allow POST and GET requests
		if ($this->request->is('post')) {
			$data = $this->request->data;
		} else {
			$data = $this->request->query;
		}
		if (!isset($data['license']) || !isset($data['host']) || !isset($data['path']) || !isset($data['version'])) {
			$result = array(
				'result' => null,
				'error' => array(
					'message' => 'Invalid request'
				));
			echo json_encode($result);
			return false;
		}

		$xlr_key = $data['license'];
		$xlr_host = $data['host'];
		$xlr_path = $data['path'];
		$xlr_version = $data['version'];

		$row = $this->License->findByLicenseKey($xlr_key);

		if (!isset($row['License'])) {
			$result = array(
				'result' => null,
				'error' => array(
					'message' => 'Invalid License'
				)
			);
		} else {
			// update the license info
			$pingdata = array(
				'id' => $row['License']['id'],
				'last_ping' => date("Y-m-d H:i:s"),
				'hostname' => $xlr_host,
				'install_path' => $xlr_path,
				'version' => $xlr_version
			);
			$this->License->save($pingdata);

			if (strtotime($row['License']['time_expire']) > time() || $row['License']['time_expire'] == '0000-00-00 00:00:00') {
				$valid = true;
			} else {
				// expired
				$valid = false;
			}
			$result = array(
				'result' => array(
					'licensed_to' => $row['License']['first_name'] . ' ' . $row['License']['last_name'],
					'type' => $row['License']['license_type'],
					'created_at' => $row['License']['time_add'],
					//'expires_at' => $row['License']['time_expire'],
					'valid' => $valid
				),
				'error' => null
			);
		}
		echo json_encode($result);
	}

	/**
	 *
	 */
	public function listen_ipn() {
		$listener = new IpnListener();

		// tell the IPN listener to use the PayPal test sandbox
		$listener->use_sandbox = true;

		// try to process the IPN POST
		try {
			$listener->requirePostMethod();
			$verified = $listener->processIpn();
		} catch (Exception $e) {
			error_log($e->getMessage());
			exit(0);
		}

		// TODO: Handle IPN Response here
		if ($verified) {

			$errmsg = '';   // stores errors from fraud checks

			// 1. Make sure the payment status is "Completed"
			if ($_POST['payment_status'] != 'Completed') {
				// simply ignore any IPN that is not completed
				exit(0);
			}

			// 2. Make sure seller email matches your primary account email.
			if ($_POST['receiver_email'] != 'YOUR PRIMARY PAYPAL EMAIL') {
				$errmsg .= "'receiver_email' does not match: ";
				$errmsg .= $_POST['receiver_email']."\n";
			}

			// 3. Make sure the amount(s) paid match
			if ($_POST['mc_gross'] != '9.99') {
				$errmsg .= "'mc_gross' does not match: ";
				$errmsg .= $_POST['mc_gross']."\n";
			}

			// 4. Make sure the currency code matches
			if ($_POST['mc_currency'] != 'USD') {
				$errmsg .= "'mc_currency' does not match: ";
				$errmsg .= $_POST['mc_currency']."\n";
			}

			// TODO: Check for duplicate txn_id

			if (!empty($errmsg)) {

				// manually investigate errors from the fraud checking
				$body = "IPN failed fraud checks: \n$errmsg\n\n";
				$body .= $listener->getTextReport();
				$Email = new CakeEmail(array('from' => 'licenses@xlrstats.com', 'transport' => 'Mail'));
				$Email->from(array('licenses@xlrstats.com' => 'XLRstats License Server'))
					->to('xlr8or@xlr8or.com')
					->subject('IPN report')
					->send($body);

			} else {

				// TODO: process order here
				$body = $listener->getTextReport();
				$Email = new CakeEmail(array('from' => 'licenses@xlrstats.com', 'transport' => 'Mail'));
				$Email->from(array('licenses@xlrstats.com' => 'XLRstats License Server'))
					->to('xlr8or@xlr8or.com')
					->subject('IPN report')
					->send($body);
			}

		} else {
			// manually investigate the invalid IPN
			$body = $listener->getTextReport();
			$Email = new CakeEmail(array('from' => 'licenses@xlrstats.com', 'transport' => 'Mail'));
			$Email->from(array('licenses@xlrstats.com' => 'XLRstats License Server'))
				->to('xlr8or@xlr8or.com')
				->subject('IPN report')
				->send($body);
		}
	}
}
